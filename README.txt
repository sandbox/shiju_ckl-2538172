﻿$Id: README.txt,v 1.0 2015/08/05 $

Installation
------------------------------------------------------------------------------
 1. Copy the module in sites/all/modules folder.

 2. Enable the "ITrInno Video Google Tracking" module from the module administration page
   (Administer >> Modules).
   
 3. Configure the module (see "Configuration" below).   

Configuration
------------------------------------------------------------------------------
 1. On the settings page ("Administration » Configuration » System » Google Analytics » Brightcove tracking settings") 
    you can enable the events which you need for tracking in google analytics.
	
NOTES	
------------------------------------------------------------------------------
 1. In Brightcove video integration code, following parameters should be included if "ItrinnoBrightcovePlayback" module is not installed
 
 <param name="includeAPI" value="true" /> 
 <param name="templateLoadHandler" value="myTemplateLoaded" />

Note: If templateLoadHandler function is already used, then call myTemplateLoaded(experienceID); in your custom templateLoadHandler function

and if the site used "ItrinnoBrightcovePlayback" module above listed param not required to be added.

Step 2: Enable the player for coding

To enable the Video Cloud player to communicate with the HTML page hosting the JavaScript, do the following:

i) Open the Video Cloud Studio Publishing module and select a player.
ii) Click Settings.
iii) On the Global tab of the player settings editor, under Web Settings, select Enable ActionScript/JavaScript APIs to enable the player for JavaScripting.
iv) Click the Save Changes button.

------------------------------------------------------------------------------
Note: Do not include APIModules_all.js/BrightcoveExperiences_all.js in page. SmartPlayer API does not work if APIModules_all.js/BrightcoveExperiences_all.js files are included in video page.


Uninstall
------------------------------------------------------------------------------
 1. Disable the module from the module administration page (Administer  >> Modules).
 2. Uninstall the module in module administration page (Administer >> Modules >> Uninstall).


Example brightcove video publishing code:
------------------------------------------------------------------------------
<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
<object id="myExperience" class="BrightcoveExperience">
<param name="bgcolor" value="#FFFFFF" />
<param name="width" value="480" />
<param name="height" value="335" />
<param name="playerID" value="1229793604001" />
<param name="playerKey" value="AQ~~,AAAAi6Tjh8k~,R20jlJ9UP6idyP-ths6bfL4Lw8zBLj5G" />
<param name="isVid" value="true" />
<param name="isUI" value="true" />
<param name="dynamicStreaming" value="true" />
<param name="includeAPI" value="true" />
<param name="templateLoadHandler" value="myTemplateLoaded" />
<param name="@videoPlayer" value="601206666001" />
</object>
<script type="text/javascript">brightcove.createExperiences();</script>
