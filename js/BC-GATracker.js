
BCHTML5GATracker = {};

var gasettings = Drupal.settings.itrinno_video_tracking;

var media_play = gasettings.media_play;
var player_load = gasettings.player_load;
var media_pause = gasettings.media_pause;
var media_resume = gasettings.media_resume;
var media_complete = gasettings.media_complete;
var referrer_url = gasettings.referrer_url;
var video_load = gasettings.video_load;
var seeked_forward = gasettings.seeked_forward;
var seeked_backward = gasettings.seeked_backward;
var milestone_passed = gasettings.milestone_passed;
var gatrackid = gasettings.googleanalytics_key;
BCHTML5GATracker.GAACCOUNT_ID = gatrackid;

BCHTML5GATracker.VERSION = "1.0.0";
BCHTML5GATracker.REFERRER_URL = "Referrer URL: ";
BCHTML5GATracker.PLAYER_NAME = "Brightcove Player";
BCHTML5GATracker.PLAYER_ID = "";
BCHTML5GATracker.PLAYER_LOAD = "Player Load";
BCHTML5GATracker.VIDEO_LOAD = "Video Load";
BCHTML5GATracker.MEDIA_BEGIN = "Media Begin";
BCHTML5GATracker.MEDIA_PAUSE = "Media Pause";
BCHTML5GATracker.MEDIA_RESUME = "Media Resume";
BCHTML5GATracker.MEDIA_COMPLETE = "Media Complete";
BCHTML5GATracker.MEDIA_ABANDONED = "Media Abandoned";
BCHTML5GATracker.SEEK_FORWARD = "Seeked Forward";
BCHTML5GATracker.SEEK_BACKWARD = "Seeked Backward";
BCHTML5GATracker.MILESTONE_25 = "25% Milestone Passed";
BCHTML5GATracker.MILESTONE_50 = "50% Milestone Passed";
BCHTML5GATracker.MILESTONE_75 = "75% Milestone Passed";

BCHTML5GATracker.VIDEO_ID = "232";
BCHTML5GATracker.VIDEO = "";

var _player,
        _experienceModule,
        _videoPlayerModule,
        _cuePointsModule,
        _currentVideo,
        _customVideoID,
        _currentPosition,
        _previousTimestamp,
        _timeWatched,
        _storedTimeWatched = {}; //stored in milliseconds
_storedTimeWatched.data = {};


//flags for tracking
var _mediaBegin = false,
        _mediaComplete = true,
        _mediaPaused = false,
        _mediaSeeking = false,
        _trackSeekForward = false,
        _trackSeekBackward = false;

//GA    
if (typeof ga === undefined && typeof ga!== 'function') {
    var _gaq = _gaq || [];
}

function myTemplateLoaded(experienceID) {
    _player = brightcove.api.getExperience(experienceID);
    _videoPlayerModule = _player.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
    _experienceModule = _player.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
    _cuePointsModule = _player.getModule(brightcove.api.modules.APIModules.CUE_POINTS);
    _experienceModule.addEventListener(brightcove.api.events.ExperienceEvent.TEMPLATE_READY, BCHTML5GATracker.onTemplateReady);

    //initialise GA
    BCHTML5GATracker.initGA();
    //set loaded player
    BCHTML5GATracker.setPlayerName();
}

BCHTML5GATracker.sendEventAnalytic = function (ga_category, ga_action, ga_label) {
    if (typeof ga !== undefined && typeof ga === 'function') {
        ga('send', 'event', ga_category, ga_action, ga_label);
    } else {
        _gaq.push(['_trackEvent', ga_category, ga_action, ga_label]);
    }
}

BCHTML5GATracker.onTemplateReady = function () {
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.CHANGE, BCHTML5GATracker.onMediaChange);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.BEGIN, BCHTML5GATracker.onMediaBegin);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.PLAY, BCHTML5GATracker.onMediaPlay);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.STOP, BCHTML5GATracker.onMediaStop);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.PROGRESS, BCHTML5GATracker.onMediaProgress);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.SEEK_NOTIFY, BCHTML5GATracker.onMediaSeek);
    _videoPlayerModule.addEventListener(brightcove.api.events.MediaEvent.COMPLETE, BCHTML5GATracker.onMediaComplete);
    _cuePointsModule.addEventListener(brightcove.api.events.CuePointEvent.CUE, BCHTML5GATracker.onCuePoint);

    // get currently loaded video DTO and create cue points
    _videoPlayerModule.getCurrentVideo(function (video) {
        BCHTML5GATracker.createCuePoints(video);
    });

    var referrerURL = document.referrer;
    if (player_load == 1) {
        BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.PLAYER_LOAD, BCHTML5GATracker.PLAYER_ID + '-' + referrerURL);
        //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.PLAYER_LOAD, BCHTML5GATracker.PLAYER_ID+'-'+referrerURL]);
    }
};


BCHTML5GATracker.onMediaChange = function () {
    _videoPlayerModule.getCurrentVideo(function (video) {
        BCHTML5GATracker.createCuePoints(video);
    });

    if (video_load == 1) {
        _videoPlayerModule.getCurrentVideo(function (video) {
            _currentVideo = video;
            _customVideoID = video.id + " | " + video.displayName;
            //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.VIDEO_LOAD, _customVideoID]);
            BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.VIDEO_LOAD, _customVideoID);
        });
    }
    _previousTimestamp = new Date().getTime();
    _timeWatched = 0;

    _mediaComplete = false;
    _mediaBegin = true;
};


BCHTML5GATracker.onMediaBegin = function () {
    /*_videoPlayerModule.getCurrentVideo(function (video) {
     _storedTimeWatched.data.abandonedVideo = video;
     });*/

    //tracks even if referrer URL is not available
    var referrerURL = document.referrer;
    var trackingAction = BCHTML5GATracker.REFERRER_URL + referrerURL;
    if (referrer_url == 1) {
        _videoPlayerModule.getCurrentVideo(function (video) {
            _currentVideo = video;
            _customVideoID = video.id + " | " + video.displayName;
            //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, trackingAction, _customVideoID]);
            BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, trackingAction, _customVideoID);
        });
    }

    if (!_mediaBegin) {
        if (media_play == 1) {
            _videoPlayerModule.getCurrentVideo(function (video) {
                _currentVideo = video;
                _customVideoID = video.id + " | " + video.displayName;
                //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_BEGIN, _customVideoID]);
                BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_BEGIN, _customVideoID);
            });
        }

        _previousTimestamp = new Date().getTime();
        _timeWatched = 0;
        _mediaComplete = false;
        _mediaBegin = true;
    }
};

BCHTML5GATracker.onMediaPlay = function () {
    if (_mediaPaused) {
        _mediaPaused = false;
        if (media_resume == 1) {
            _videoPlayerModule.getCurrentVideo(function (video) {
                _currentVideo = video;
                _customVideoID = video.id + " | " + video.displayName;
                //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_RESUME, _customVideoID]);
                BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_RESUME, _customVideoID);
            });
        }
    }
};


BCHTML5GATracker.onMediaStop = function () {
    if (!_mediaPaused) {
        _mediaPaused = true;
        if (media_pause == 1) {
            _videoPlayerModule.getCurrentVideo(function (video) {
                _currentVideo = video;
                _customVideoID = video.id + " | " + video.displayName;
                //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_PAUSE, _customVideoID]);
                BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_PAUSE, _customVideoID);
            });
        }
    }
};


BCHTML5GATracker.onMediaProgress = function (pEvent) {
    _currentPosition = pEvent.position;
    //update tracked time
    BCHTML5GATracker.updateTrackedTime();

    /*
     This will track the media complete event when the user has watched 98% or more of the video. 
     Why do it this way and not use the Smart Player API's event? The mediaComplete event will 
     only fire once, so if a video is replayed, it won't fire again.
     */
    if (pEvent.position / pEvent.duration > .98 && !_mediaComplete) {
        BCHTML5GATracker.onMediaComplete(pEvent);

        //empty these since we don't want to track it when someone comes back
        _storedTimeWatched.data.abandonedVideo = null;
        _storedTimeWatched.data.abandonedTimeWatched = null;

    }
    //track seek events
    if (!_mediaSeeking) {
        if (_trackSeekForward) {
            if (seeked_forward == 1) {
                _videoPlayerModule.getCurrentVideo(function (video) {
                    _currentVideo = video;
                    _customVideoID = video.id + " | " + video.displayName;
                    //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.SEEK_FORWARD, _customVideoID]);
                    BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.SEEK_FORWARD, _customVideoID);
                });
            }
            _trackSeekForward = false;
        }

        if (_trackSeekBackward) {
            if (seeked_backward == 1) {
                _videoPlayerModule.getCurrentVideo(function (video) {
                    _currentVideo = video;
                    _customVideoID = video.id + " | " + video.displayName;
                    //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.SEEK_BACKWARD, _customVideoID]);
                    BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.SEEK_BACKWARD, _customVideoID);
                });
            }
            _trackSeekBackward = false;
        }
    } else {
        _mediaSeeking = false;
    }

};

BCHTML5GATracker.onMediaSeek = function (pEvent) {

    if (pEvent.position > _currentPosition) {
        _trackSeekForward = true;
    } else {
        _trackSeekBackward = true;
    }

    _mediaSeeking = true;

};


BCHTML5GATracker.onCuePoint = function (pEvent) {
    if (pEvent.cuePoint.type == 2 && pEvent.cuePoint.name == "milestone" && milestone_passed == 1) {
        switch (pEvent.cuePoint.metadata) {
            case "25%":
                _videoPlayerModule.getCurrentVideo(function (video) {
                    _currentVideo = video;
                    _customVideoID = video.id + " | " + video.displayName;
                    //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_25, _customVideoID]);
                    BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_25, _customVideoID);
                });
                break;
            case "50%":
                _videoPlayerModule.getCurrentVideo(function (video) {
                    _currentVideo = video;
                    _customVideoID = video.id + " | " + video.displayName;
                    //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_50, _customVideoID]);
                    BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_50, _customVideoID);
                });
                break;
            case "75%":
                _videoPlayerModule.getCurrentVideo(function (video) {
                    _currentVideo = video;
                    _customVideoID = video.id + " | " + video.displayName;
                    //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_75, _customVideoID]);
                    BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MILESTONE_75, _customVideoID);
                });
                break;
        }
    }
};

BCHTML5GATracker.initGA = function () {
    BCHTML5GATracker.GAACCOUNT_ID = (BCHTML5GATracker.GAACCOUNT_ID == null) ? BCHTML5GATracker.getParamValue('GAID') : BCHTML5GATracker.GAACCOUNT_ID;
    if (BCHTML5GATracker.GAACCOUNT_ID == null) {
        console.log("The Google Analytics account number has not been defined. This is required for the analytics plugin to function properly.");
    } else {
        console.log("GA Account Number: " + BCHTML5GATracker.GAACCOUNT_ID);
        if (typeof ga !== undefined && typeof ga === 'function') {
            ga('create', BCHTML5GATracker.GAACCOUNT_ID, 'auto');
            ga('send', 'pageview');
        } else {
            _gaq.push(['_setAccount', BCHTML5GATracker.GAACCOUNT_ID]);
            _gaq.push(['_trackPageview']);
        }

        try {
            // determine whether to include the normal or SSL version
            var gaURL = (location.href.indexOf('https') == 0 ? 'https://ssl' : 'http://www');
            if (typeof ga !== undefined && typeof ga === 'function') {
                gaURL += '.google-analytics.com/analytics.js';
            } else {
                gaURL += '.google-analytics.com/ga.js';
            }
            // include the ga.js
            $.getScript(gaURL);
        } catch (error) {
            console.log('Failed to load ga.js:' + error);
        }
    }
};


BCHTML5GATracker.getParamValue = function (key) {
    //check plugin params for the value
    var pluginURL = experienceJSON.data.configuredProperties.plugins.list;
    var vars = [], hash;
    var hashes = pluginURL.slice(pluginURL.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars[key];
};


BCHTML5GATracker.setPlayerName = function () {
    _experienceModule.getExperienceID(BCHTML5GATracker.setPlayerID);
    var playerName = unescape(BCHTML5GATracker.getParamValue('PlayerName'));
    var playerId = _experienceModule.getExperienceID();
    if (playerName != "undefined") {
        BCHTML5GATracker.PLAYER_NAME = playerName + " ID " + playerId;
    } else {
        BCHTML5GATracker.PLAYER_NAME = BCHTML5GATracker.PLAYER_NAME + " ID " + playerId;
    }
};

BCHTML5GATracker.getLoadedVideo = function (videoDTO) {
    var loadedVideo = videoDTO.id + " | " + videoDTO.displayName;
    return loadedVideo;
};


BCHTML5GATracker.updateTrackedTime = function () {
    var currentTimestamp = new Date().getTime();
    var timeElapsed = (currentTimestamp - _previousTimestamp) / 1000;
    _previousTimestamp = currentTimestamp;
    //check if it's more than 2 seconds in case the user paused or changed their local time or something
    if (timeElapsed < 2) {
        _timeWatched += timeElapsed;
    }

    //update time watched in case the user bails out before mediaComplete
    if (!_mediaComplete) {//make sure mediaComplete hasn't fired yet, otherwise it gets set to null and the repopulated: not what we want
        _storedTimeWatched.data.abandonedTimeWatched = _timeWatched; //automatically gets flushed when flash player is closed	
    }
};


BCHTML5GATracker.onMediaComplete = function (pEvent) {
    if (!_mediaComplete) {
        _mediaComplete = true;
        _mediaBegin = false;
        //alert(_timeWatched);
        if (media_complete == 1) {
            _videoPlayerModule.getCurrentVideo(function (video) {
                _currentVideo = video;
                _customVideoID = video.id + " | " + video.displayName;
                //_gaq.push(['_trackEvent', BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_COMPLETE, _customVideoID, Math.round(_timeWatched)]);
                BCHTML5GATracker.sendEventAnalytic(BCHTML5GATracker.PLAYER_NAME, BCHTML5GATracker.MEDIA_COMPLETE, _customVideoID, Math.round(_timeWatched));
            });
        }
    }
};

BCHTML5GATracker.createCuePoints = function (pVideo) {
    var percent25 = {
        type: 2, //chapter cue point
        name: "milestone",
        metadata: "25%",
        time: (pVideo.length / 1000) * .25
    };
    var percent50 = {
        type: 2, //chapter cue point
        name: "milestone",
        metadata: "50%",
        time: (pVideo.length / 1000) * .5
    };
    var percent75 = {
        type: 2, //chapter cue point
        name: "milestone",
        metadata: "75%",
        time: (pVideo.length / 1000) * .75
    };

    _cuePointsModule.addCuePoints(pVideo.id, [percent25, percent50, percent75]);
};

BCHTML5GATracker.onGetCurrentVideoResult = function (videoDTO) {
    //console.log(video);
    BCHTML5GATracker.VIDEO = videoDTO;
    BCHTML5GATracker.VIDEO_ID = videoDTO.id + " | " + videoDTO.displayName;
};
BCHTML5GATracker.setPlayerID = function (result) {
    BCHTML5GATracker.PLAYER_ID = result;
};

